Tools
=====

.. autofunction:: file_adds.create_folder
.. autofunction:: serie_transf.efficiency_criteria
.. autofunction:: serie_transf.date_range
.. autofunction:: tif_adds.export_tif
.. autofunction:: tif_adds.reproject_tif
.. autofunction:: tif_adds.reproject_coord
.. autofunction:: tif_adds.reproject_shp
.. autofunction:: tif_features.basin_area
.. autofunction:: tif_masks.clip_tif
.. autofunction:: tif_masks.mask_by_dem
.. autofunction:: to_plot.plot_params

Visualisation 2D
************************
.. automodule:: visualization_watershed
    :members:
    :undoc-members:
    :show-inheritance:


Visualisation 3D
************************
.. autoclass:: export_vtuvtk.VTK
    :members:
    :undoc-members:
    :show-inheritance:


