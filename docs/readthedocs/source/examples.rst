Examples
========

.. nbgallery::
    notebooks/example_00
    notebooks/example_01
    notebooks/example_02
    notebooks/example_03
    notebooks/example_04
    notebooks/example_05
    notebooks/example_06
    notebooks/example_07
    notebooks/example_08
